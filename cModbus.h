#pragma once

#include "modbus.h"
#include <unistd.h>
#include <algorithm>
#include <chrono>
#include "wx/wx.h"
#include "cModbusRTU.h"
#include "cMain.h"
#include "cEntry.h"
#include "constants.h"

using namespace std;

class cTimerHighRes;
class cThreadReadPSU;
class cThreadWritePSU;
class cThreadSetPolarity;
class cMain;
class cEntry;

/* Modbus Gateway class */
class cModbus
{
	public:
		cModbus(cEntry*, cMain*);
		void CloseModbus(void);
		void SetPowerUnitCurrent(uint8_t, float);
		void SetPowerUnitPolarity(uint8_t, bool);

	public: //class pointers and flags
		cMain* pMain;
		bool bStartMain;

	public: //modbus
		int32_t mbAddress;
		wxString mbPort;
		modbus_t* ctxRTU;
		modbus_mapping_t* mb_mapping;
		int32_t ctRTU;

	public: //critical sections
		wxCriticalSection mb_guard; //prevent multiple Modbus RTU operations
		wxCriticalSection local_guard; //prevent simultaneous local data access
		wxCriticalSection remote_guard; //prevent simultaneous remote data access

	public: //SHARED DATA
		float Current[NUM_POWER_UNITS] = { 0 };
		uint16_t STA[4] = { 0 };
		uint16_t ZNA;
		uint16_t ErrorsEcho;
		uint16_t ErrorsUART;
		uint16_t ErrorsCRLF;
		bool bPolarityErrors[NUM_POWER_UNITS - 2] = { false };
		bool bModbusError;

	protected: //multithreading
		cThreadReadPSU* mbThreadReadPSU;
		cThreadWritePSU* mbThreadWritePSU;
		cThreadSetPolarity* mbThreadSetPolarity;
		friend class cThreadReadPSU;
		friend class cThreadWritePSU;
		friend class cThreadSetPolarity;

	private: //bit register addressing
		int32_t STA_Address[4] = { 0 };
		int32_t ZNA_Address;
};
