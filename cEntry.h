#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "Images/icon.xpm"
#include <boost/algorithm/string.hpp>
#include <string>
#include <vector>
#include "modbus.h"
#include "wx/wx.h"
#include "wx/log.h"
#include "wx/textfile.h"
#include "wx/snglinst.h"
#include "sys/socket.h"
#include "arpa/inet.h"
#include "unistd.h"
#include "regex.h"
#include "cMain.h"
#include "cModbus.h"
#include "constants.h"

class cMain;
class cModbus;

using namespace std;

class cEntry : public wxApp
{
public:
	cEntry();

public:
    virtual bool OnInit() override;
    virtual int OnExit() override;

public: //class pointers
    cMain* pMain;
    cModbus* mbMaster;
    wxSingleInstanceChecker* wxChecker;

public: //Modbus configuration
    int32_t mbAddress;
    wxString mbPort;

public: //Bit Register addresses
	int32_t STA_Address[4] = { 0 };
	int32_t ZNA_Address;

private: //Configuration file variables
    wxTextFile tConfigFile;
    wxString filename;
    wxString strConfig;

private: //other variables
    wxString temp;
    bool bConfigError;
};

#pragma GCC diagnostic pop
