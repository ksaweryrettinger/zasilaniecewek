#include "cModbusRTU.h"

cThreadReadPSU::cThreadReadPSU(cModbus *mbMaster) : wxThread(wxTHREAD_DETACHED)
{
	//Initialize variables
    this->mbMaster = mbMaster;
    rc = -1;

	//Create new RTU configuration
    mbMaster->ctxRTU = modbus_new_rtu(mbMaster->mbPort.mb_str(), MB_BAUD, 'E', 8, 1);
	modbus_set_slave(mbMaster->ctxRTU, mbMaster->mbAddress);
	modbus_rtu_set_serial_mode(mbMaster->ctxRTU, MODBUS_RTU_RS232);
	modbus_set_response_timeout(mbMaster->ctxRTU, MB_RESPONSE_TIMEOUT_SEC, MB_RESPONSE_TIMEOUT_US);
	ct = modbus_connect(mbMaster->ctxRTU);

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		mbMaster->ctRTU = ct;
	}

	/****************************** Initialize UART Modules ************************************/

	if (ct == -1) //Modbus connection error
    {
    	//Signal program to start main window
    	if (!mbMaster->bStartMain) mbMaster->bStartMain = true;

    	//Store Modbus error in remote data
    	{
    		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);
    		mbMaster->mb_mapping->tab_registers[MB_HOLDING_NUMREG] = (uint16_t) true;
    	}
    }
}

wxThread::ExitCode cThreadReadPSU::Entry()
{
	if (ct != -1)
	{
		while (!TestDestroy())
		{
			/************************************ Modbus RTU Communication ******************************/

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);

				//Clear Modbus errors
				rc = 0;

				//Read Holding Registers
				rc += modbus_read_registers(mbMaster->ctxRTU, MB_HOLDING_ADDRESS, MB_HOLDING_NUMREG - 5, RegHoldingTemp);

				//Read 16-bit registers
				rc += modbus_read_registers(mbMaster->ctxRTU, mbMaster->STA_Address[0], 1, &RegHoldingTemp[HOLDING_STA0 - 1]);
				rc += modbus_read_registers(mbMaster->ctxRTU, mbMaster->STA_Address[1], 1, &RegHoldingTemp[HOLDING_STA1 - 1]);
				rc += modbus_read_registers(mbMaster->ctxRTU, mbMaster->STA_Address[2], 1, &RegHoldingTemp[HOLDING_STA2 - 1]);
				rc += modbus_read_registers(mbMaster->ctxRTU, mbMaster->STA_Address[3], 1, &RegHoldingTemp[HOLDING_STA3 - 1]);
				rc += modbus_read_registers(mbMaster->ctxRTU, mbMaster->ZNA_Address, 1, &RegHoldingTemp[HOLDING_ZNA - 1]);
			}

			/************************************ Remote Data *******************************************/

			{
				wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);

				//Copy data to Modbus mapping
				for (uint8_t i = 0; i < MB_HOLDING_NUMREG; i++) mbMaster->mb_mapping->tab_registers[i] = RegHoldingTemp[i];

				//Modbus RTU errors
				if (rc != MB_HOLDING_NUMREG) mbMaster->mb_mapping->tab_registers[HOLDING_MBERRORS - 1] = (uint16_t) true;
				else mbMaster->mb_mapping->tab_registers[HOLDING_MBERRORS - 1] = (uint16_t) false;
			}

			/************************************ Local Data *******************************************/

			{
				wxCriticalSectionLocker local_lock(mbMaster->local_guard);

				//Modbus RTU errors
				if (rc != MB_HOLDING_NUMREG) mbMaster->bModbusError = true;
				else mbMaster->bModbusError = false;

				//PSU Communication Errors
				mbMaster->ErrorsEcho = RegHoldingTemp[HOLDING_ECHO_ERRORS - 1];
				mbMaster->ErrorsUART = RegHoldingTemp[HOLDING_UART_ERRORS - 1];
				mbMaster->ErrorsCRLF = RegHoldingTemp[HOLDING_CRLF_ERRORS - 1];

				//Bit Registers
				mbMaster->STA[0] = RegHoldingTemp[HOLDING_STA0 - 1];
				mbMaster->STA[1] = RegHoldingTemp[HOLDING_STA1 - 1];
				mbMaster->STA[2] = RegHoldingTemp[HOLDING_STA2 - 1];
				mbMaster->STA[3] = RegHoldingTemp[HOLDING_STA3 - 1];
				mbMaster->ZNA = RegHoldingTemp[HOLDING_ZNA - 1];

				//BCD Current and Polarity Errors
				for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
				{
					//Convert BCD current to floating point
					cCurrent[0] = ((RegHoldingTemp[i] >> 12) & 0xF) + '0';
					cCurrent[1] = ((RegHoldingTemp[i] >> 8) & 0xF) + '0';
					cCurrent[2] = ((RegHoldingTemp[i] >> 4) & 0xF) + '0';
					cCurrent[3] = (RegHoldingTemp[i] & 0xF) + '0';
					cCurrent[4] = '\n';
					mbMaster->Current[i] = atof(cCurrent);

					if (i < NUM_POWER_UNITS - 2)
					{
						mbMaster->bPolarityErrors[i] = ((mbMaster->STA[ADR[i][1]] >> ADR[i][2]) & 1)
								== ((mbMaster->STA[ADR[i][3]] >> ADR[i][4]) & 1);
					}
				}
			}

			/************************************ Other ************************************************/

			//Signal program to start main window
			if (!mbMaster->bStartMain) mbMaster->bStartMain = true;

			//Pause between read commands
			wxThread::Sleep(MB_RTU_SLEEP);
		}
	}

	return (wxThread::ExitCode) 0;
}

cThreadReadPSU::~cThreadReadPSU()
{
	wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
	modbus_close(mbMaster->ctxRTU);
	modbus_free(mbMaster->ctxRTU);
	mbMaster->mbThreadReadPSU = NULL;
}

cThreadWritePSU::cThreadWritePSU(cModbus *mbMaster, uint8_t unitID, float current) : wxThread(wxTHREAD_DETACHED)
{
    this->mbMaster = mbMaster;
    this->unitID = unitID;
    this->current = current;
    currentBCD = 0;
    rc = -1;

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		ct = mbMaster->ctRTU;
	}
}

wxThread::ExitCode cThreadWritePSU::Entry()
{
	wxString strCurrent = wxString::Format(wxT("%.1f"), current);

	//Construct BCD message
	if (strCurrent.length() == 3)
	{
		currentBCD |= (strCurrent[0] - '0') << 4;
		currentBCD |= (strCurrent[2] - '0');
	}
	else if (strCurrent.length() == 4)
	{
		currentBCD |= (strCurrent[0] - '0') << 8;
		currentBCD |= (strCurrent[1] - '0') << 4;
		currentBCD |= (strCurrent[3] - '0');
	}
	else if (strCurrent.length() == 5)
	{
		currentBCD |= (strCurrent[0] - '0') << 12;
		currentBCD |= (strCurrent[1] - '0') << 8;
		currentBCD |= (strCurrent[2] - '0') << 4;
		currentBCD |= (strCurrent[4] - '0');
	}

	{
		//Send new current value
		wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
		if (ct != -1) rc = modbus_write_register(mbMaster->ctxRTU, MB_HOLDING_ADDRESS + (unitID - 1), currentBCD);
	}

	//Terminate thread
	return (wxThread::ExitCode) 0;
}

cThreadSetPolarity::cThreadSetPolarity(cModbus *mbMaster, uint8_t unitID, bool bSetNegative) : wxThread(wxTHREAD_DETACHED)
{
    this->mbMaster = mbMaster;
    this->bSetNegative = bSetNegative;
    this->unitID = unitID;
	rc = -1;

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		ct = mbMaster->ctRTU;
	}
}

wxThread::ExitCode cThreadSetPolarity::Entry()
{
	if (unitID <= 14)
	{
		{
			//Set new polarity
			wxCriticalSectionLocker local_lock(mbMaster->local_guard); //prevent Local data access
			if (bSetNegative) mbMaster->ZNA &= ~(1 << ADR[unitID - 1][5]); //clear bit (negative polarity)
			else mbMaster->ZNA |= 1 << ADR[unitID - 1][5]; //set bit (positive polarity)
		}

		{
			//Write new polarity settings
			wxCriticalSectionLocker mb_lock(mbMaster->mb_guard); //prevent other Modbus RTU operations
			if (ct != -1) rc = modbus_write_register(mbMaster->ctxRTU, mbMaster->ZNA_Address, mbMaster->ZNA);
		}
	}

	//Terminate thread
	return (wxThread::ExitCode) 0;
}

