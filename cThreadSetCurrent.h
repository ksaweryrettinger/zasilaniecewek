#pragma once

#include "cMain.h"
#include <algorithm>
#include <chrono>

class cMain;
class cModbus;
class cTimerHighRes;

using namespace std;

/* System clock thread */
class cThreadSetCurrent : public wxThread
{
	public:
		cThreadSetCurrent(cMain*, cModbus*, uint8_t, float);
		~cThreadSetCurrent();

	protected:
		virtual ExitCode Entry();

	private:
		void RefreshModbusData(void);
		void RefreshMainData(void);
		bool TestErrors(void);

	private:
		cMain* pMain;
		cModbus* mbMaster;

	private:
		float Current;
		uint16_t STA[4] = { 0 };
		uint16_t ZNA;
		uint16_t ErrorsEcho;
		uint16_t ErrorsUART;
		uint16_t ErrorsCRLF;
		bool bPolarityError;
		bool bModbusError;
		bool bCoolingError;

	private: //other data
		cTimerHighRes* TimerSetCurrent;
		bool bTimeout;
		bool bPolarityOK;
		float newCurrent;
		int32_t unitID;
};

class cTimerHighRes
{
	public:
		cTimerHighRes() : m_beg(clock_::now()) {}

		//Timer reset
		void reset()
		{
			m_beg = clock_::now();
		}

		//Returns time elapsed in microseconds since last reset or object creation
		double elapsed() const
		{
			return chrono::duration_cast<chrono::seconds>(clock_::now() - m_beg).count();
		}

	private:
		typedef chrono::high_resolution_clock clock_;
		chrono::time_point<clock_> m_beg;
};
