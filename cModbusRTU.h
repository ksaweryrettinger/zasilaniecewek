#pragma once

#include "modbus.h"
#include <unistd.h>
#include <algorithm>
#include <chrono>
#include "wx/wx.h"
#include "constants.h"
#include "cModbus.h"

using namespace std;

class cModbus;
class cThreadReadPSU;
class cThreadWritePSU;
class cThreadSetPolarity;
class cTimerHighRes;

/* Modbus RTU thread class */
class cThreadReadPSU : public wxThread
{
	public:
		cThreadReadPSU(cModbus*);
		~cThreadReadPSU();

	protected:
		virtual ExitCode Entry();
		cModbus* mbMaster;

	private:
		uint16_t RegHoldingTemp[MB_HOLDING_NUMREG];
		char cCurrent[5] = { 0 };
		int32_t rc;
		int32_t ct;
};

/* Temporary thread, used to write new current value to PSU */
class cThreadWritePSU : public wxThread
{
	public:
		cThreadWritePSU(cModbus*, uint8_t, float);

	protected:
		virtual ExitCode Entry();
		cModbus* mbMaster;

	private:
		float current;
		uint16_t currentBCD;
		uint8_t unitID;
		int32_t rc;
		int32_t ct;
};

/* Temporary thread, used to set new PSU polarity */
class cThreadSetPolarity : public wxThread
{
	public:
		cThreadSetPolarity(cModbus*, uint8_t, bool);

	protected:
		virtual ExitCode Entry();
		cModbus* mbMaster;

	private:
		bool bSetNegative;
		uint8_t unitID;
		int32_t rc;
		int32_t ct;
};
