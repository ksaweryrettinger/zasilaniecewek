#include "cMain.h"

/* Button clicked event */
void cMain::OnButtonConfirmSetting(wxCommandEvent& evt)
{
	//Get button ID
	int32_t btnID = evt.GetId();
	int32_t unitID = btnID - BTN_ID_START + 1;

	//Disable button during current change operation
	btnConfirmSetting[btnID - BTN_ID_START]->Disable();

	//Read current setting
	wxString strCurrent = txtCtrlCurrentSetting[unitID - 1]->GetValue();
	float newCurrent = wxAtof(strCurrent);

	//Check current limits
	if (newCurrent < (MAX_CURRENT * (-1)) && unitID <= (NUM_POWER_UNITS - 2)) newCurrent = MAX_CURRENT * (-1);
	else if (newCurrent < 0 && unitID > (NUM_POWER_UNITS - 2)) newCurrent = 0;
	else if (newCurrent > MAX_CURRENT) newCurrent = MAX_CURRENT;
	txtCtrlCurrentSetting[unitID - 1]->SetValue(wxString::Format(wxT("%.1f"), newCurrent));

	//Start new thread that handles new current setting
    threadSetCurrent[unitID - 1] = new cThreadSetCurrent(this, mbMaster, unitID, newCurrent);

    if (threadSetCurrent[unitID - 1]->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create set-current thread!");
        delete threadSetCurrent[unitID - 1];
        threadSetCurrent[unitID - 1] = NULL;
    }

    evt.Skip();
}
