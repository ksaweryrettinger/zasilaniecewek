#include "cEntry.h"

wxIMPLEMENT_APP(cEntry);

int8_t ADR[14][6] = { 0 };
int8_t CEW[20][3] = { 0 };
int32_t MAX_CURRENT = 0;

cEntry::cEntry()
{
	//Initialize variables and pointers
    pMain = nullptr;
	mbMaster = nullptr;
    wxChecker = nullptr;
    mbAddress = 0;
    ZNA_Address = 0;
    bConfigError = false;
}

bool cEntry::OnInit()
{
    wxApp::SetAppName(wxString(_T("Zasilanie Cewek Korekcyjnych")));

    {
        wxLogNull logNo; //suppress information message
        wxChecker = new wxSingleInstanceChecker(_T(".zasilaniecewek_temp"));
    }

    //Check if process already exists
    if (wxChecker->IsAnotherRunning())
    {
        wxLogError(_T("\n       Proces już istnieje!       "));
        delete wxChecker;
        wxChecker = nullptr;
        return false;
    }

	//Open configuration file
	filename = wxString::FromUTF8(getenv("HOME")) + "/eclipse-workspace/ZasilanieCewek/multizck.cnf";
	tConfigFile.Open(filename);

	string s;
	vector<string> v;

	while (!tConfigFile.Eof())
	{
		strConfig = tConfigFile.GetNextLine();

		if ((strConfig[0] != '#') && !(strConfig.IsEmpty()))
		{
			s = strConfig.ToStdString();
			boost::trim_if(s, boost::is_any_of("\t "));
			boost::split(v, s, boost::is_any_of(" \t"), boost::token_compress_on);

			if (v[0] == "MBS") //Modbus Slave Address
			{
				if (v.size() >= 2) mbAddress = wxAtoi(v[1]);
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "TTY") //Linux Serial Port
			{
				if (v.size() >= 2) mbPort = v[1];
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "ZNA") //ZNA Bit Register
			{
				if (v.size() >= 2) ZNA_Address = strtol(("0x" + v[1]).c_str(), NULL, 16);
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "STA") //STA Bit Registers
			{
				if (v.size() >= 3) STA_Address[wxAtoi(v[1])] = strtol(("0x" + v[2]).c_str(), NULL, 16);
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "ADR") //PSU Configuration
			{
				if (v.size() >= 8)
				{
					ADR[wxAtoi(v[1]) - 1][0] = wxAtoi(v[2]);
					ADR[wxAtoi(v[1]) - 1][1] = wxAtoi(v[3]);
					ADR[wxAtoi(v[1]) - 1][2] = wxAtoi(v[4]);
					ADR[wxAtoi(v[1]) - 1][3] = wxAtoi(v[5]);
					ADR[wxAtoi(v[1]) - 1][4] = wxAtoi(v[6]);
					ADR[wxAtoi(v[1]) - 1][5] = wxAtoi(v[7]);
				}
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "CEW") //PSU Coil Configuration
			{
				if (v.size() >= 5)
				{
					CEW[wxAtoi(v[1]) - 1][0] = wxAtoi(v[2]);
					CEW[wxAtoi(v[1]) - 1][1] = wxAtoi(v[3]);
					CEW[wxAtoi(v[1]) - 1][2] = wxAtoi(v[4]);
				}
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "MAX") //Maximum Current
			{
				if (v.size() >= 2) MAX_CURRENT = wxAtoi(v[1]);
				else
				{
					bConfigError = true;
					break;
				}
			}
		}
	}

	if (bConfigError)
	{
		 wxLogError(_T("\n       Błąd w pliku konfiguracyjnym!      "));
		 return false;
	}

    //Create Main window
    pMain = new cMain(this);
	pMain->SetIcon(wxICON(icon));

	//Start Modbus master
	mbMaster = new cModbus(this, pMain);
	while (!mbMaster->bStartMain) {};
	wxThread::Sleep(1000);
	pMain->Show();

	return true;
}

int cEntry::OnExit()
{
    //Close Modbus gateway and free resources
    mbMaster->CloseModbus();

    //Free pointers
    delete mbMaster;
    delete wxChecker;
    mbMaster = nullptr;
    wxChecker = nullptr;

    return 0;
}
