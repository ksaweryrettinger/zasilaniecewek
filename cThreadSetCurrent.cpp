#include "cThreadSetCurrent.h"

/*	This thread does the following:
 *
 *	  - Check if polarity changed.
 *  	- If polarity didn't change, send command with new Current setting.
 *  	- If polarity did change:
 *  		- Set Current to zero and wait until it reaches zero.
 *  		- Change polarity of power unit.
 *      	- Wait for confirmation of polarity switch.
 *			- Apply new Current value.
 */

cThreadSetCurrent::cThreadSetCurrent(cMain* pMain, cModbus* mbMaster, uint8_t unitID, float newCurrent) : wxThread(wxTHREAD_DETACHED)
{
	this->pMain = pMain;
	this->mbMaster = mbMaster;
	this->unitID = unitID;
	this->newCurrent = newCurrent;
	this->Current = 0;
	bTimeout = false;
	bPolarityOK = false;
	TimerSetCurrent = new cTimerHighRes();

	RefreshModbusData();
	RefreshMainData();
}

cThreadSetCurrent::~cThreadSetCurrent()
{
	wxCriticalSectionLocker main_lock(pMain->main_guard);
	pMain->threadSetCurrent[unitID - 1] = NULL;
}

wxThread::ExitCode cThreadSetCurrent::Entry()
{
	if (unitID <= (NUM_POWER_UNITS - 2) && ((Current > 0 && newCurrent < 0) || (Current < 0 && newCurrent > 0))) //polarity changed
	{
		//Set current to zero
		mbMaster->SetPowerUnitCurrent(unitID, 0);

		//Start timeout timer
		TimerSetCurrent->reset();

		//Wait for current to reach zero
		while (Current != 0)
		{
			wxThread::Sleep(ZERO_CURRENT_DELAY);

			RefreshModbusData();
			RefreshMainData();
			if (TestErrors()) return (wxThread::ExitCode) 0;
		}

		//Switch polarity
		mbMaster->SetPowerUnitPolarity(unitID, (newCurrent < 0));

		//Start timeout timer
		TimerSetCurrent->reset();

		//Wait for polarity switch
		while (!bPolarityOK)
		{
			wxThread::Sleep(SWITCH_POLARITY_DELAY);

			RefreshModbusData();
			RefreshMainData();
			if (TestErrors()) return (wxThread::ExitCode) 0;

			//Check if polarity changed correctly
			if (newCurrent < 0 && !bPolarityError && ((STA[ADR[unitID - 1][1]] >> ADR[unitID - 1][2]) & 1)) bPolarityOK = true;
			else if (newCurrent > 0 && !bPolarityError && ((STA[ADR[unitID - 1][3]] >> ADR[unitID - 1][4]) & 1)) bPolarityOK = true;
		}

		mbMaster->SetPowerUnitCurrent(unitID, abs(newCurrent)); //apply new current (absolute value)
	}
	else if (newCurrent != Current)//POLARITY UNCHANGED
	{
		mbMaster->SetPowerUnitCurrent(unitID, abs(newCurrent)); //apply new current (absolute value)
	}

	//Clear timeout errors
	pMain->bSetCurrentTimeout[unitID - 1] = false;

	return (wxThread::ExitCode) 0;
}

void cThreadSetCurrent::RefreshModbusData(void)
{
	//Copy Modbus data
	wxCriticalSectionLocker local_lock(mbMaster->local_guard);

	bModbusError = mbMaster->bModbusError;

	//Current reading
	Current = mbMaster->Current[unitID - 1];

	//Polarity errors
	if (unitID <= NUM_POWER_UNITS - 2) bPolarityError = mbMaster->bPolarityErrors[unitID - 1];

	//Bit registers
	STA[0] = mbMaster->STA[0];
	STA[1] = mbMaster->STA[1];
	STA[2] = mbMaster->STA[2];
	STA[3] = mbMaster->STA[3];
	ZNA = mbMaster->ZNA;

	//Errors
	ErrorsEcho = mbMaster->ErrorsEcho;
	ErrorsUART = mbMaster->ErrorsUART;
	ErrorsCRLF = mbMaster->ErrorsCRLF;
}

void cThreadSetCurrent::RefreshMainData(void)
{
	wxCriticalSectionLocker main_lock(pMain->main_guard);
	bCoolingError = pMain->bCoolingError[unitID - 1];
}

bool cThreadSetCurrent::TestErrors(void)
{
	//Terminate thread on timeout
	if (TimerSetCurrent->elapsed() > ZERO_CURRENT_TIMEOUT)
	{
		wxCriticalSectionLocker main_lock(pMain->main_guard);
		pMain->bSetCurrentTimeout[unitID - 1] = true; //set timeout error in main thread
		return true;
	}

	//Terminate thread in the event of errors
	if (bModbusError || ((ErrorsCRLF >> (unitID - 1)) & 1)
			|| ((ErrorsUART >> (unitID - 1)) & 1) || ((ErrorsEcho >> (unitID - 1)) & 1)) return true;

	//Terminate thread if destroyed externally
	if (TestDestroy()) return true;

	//Terminate thread in the event of coil cooling problems
	if (newCurrent != 0 && bCoolingError) return true;

	//No errors
	return false;
}

