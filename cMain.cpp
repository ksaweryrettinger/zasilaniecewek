#include "cMain.h"

wxBEGIN_EVENT_TABLE(cMain, wxFrame)
	EVT_MENU(wxID_INFO, cMain::OnMenuClickedInformation)
	EVT_MENU(wxID_NETWORK, cMain::OnMenuClickedMode)
	EVT_MENU(wxID_CLOSE, cMain::OnMenuClickedExit)
	EVT_COMMAND(wxID_ANY, wxEVT_DISPLAY_UPDATE, cMain::OnRefreshDisplay)
    EVT_CLOSE(cMain::OnClose)
wxEND_EVENT_TABLE()

/* Frame constructor */
cMain::cMain(cEntry* pEntry) : wxFrame(nullptr, wxID_ANY, " ", wxPoint(0, 0), wxSize(1110, 900), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
    //FRAME TITLE AND COLOUR
    this->SetTitle(_T("Zasilanie Cewek Korekcyjnych [Sterowanie Zdalne]"));
    this->SetBackgroundColour(wxColour(230, 230, 230));

    //CONSTRUCTOR VARIABLES
    mbMaster = nullptr;
    pInformation = nullptr;
    this->pEntry = pEntry;

    //Other variables initialization
    fill_n(threadSetCurrent, NUM_POWER_UNITS, nullptr);
    informationWindowIsOpen = false;

	//MENUS
	mMenuBar = new wxMenuBar();
	mMenuBar->SetFont(wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
	mHelpMenu = new wxMenu();
	mOptionsMenu = new wxMenu();

	if (mHelpMenu != nullptr)
	{
		mHelpMenu->Append(wxID_INFO, _T("Informacje"));
		mOptionsMenu->Append(wxID_NETWORK, _T("Sterowanie Lokalne"));
		mOptionsMenu->Append(wxID_CLOSE, _T("Koniec Pracy"));
		mMenuBar->Append(mOptionsMenu, _T("Opcje"));
		mMenuBar->Append(mHelpMenu, _T("Pomoc"));
		SetMenuBar(mMenuBar);
	}

	//USER MESSAGES
	msgPSUExcluded = wxString(_T("Komunikacja\nWyłączona"));
    msgModbusError = wxString(_T("Błąd Modbus."));
    msgEchoError = wxString(_T("Błąd ECHO."));
    msgUARTError = wxString(_T("Błąd UART."));
    msgCRLFError = wxString(_T("Brak odpowiedzi."));
	msgCoolingError = wxString(_T("Błąd chłodzenia."));
	msgPolarityError = wxString(_T("Błąd polaryzacji."));
	msgSetCurrentTimeout = wxString(_T("Błąd zmiany prądu."));

	//MODBUS VARIABLES
	ZNA = 0;
	ErrorsEcho = 0;
	ErrorsUART = 0;
	ErrorsCRLF = 0;
	bModbusError = false;;

	//OTHER VARIABLES
	eUserMode = Remote;

	//TEMPORARY VARIABLES
	wxStaticText* coil1;
	wxStaticText* coil2;
	int32_t CoilNum = 0;
	int32_t PSUCoils[2] = { 0 };
	wxStaticLine* lineH1;

    //PANELS, BUTTONS and TEXT
	for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
	{
		//Draw PSU panels
		if (i < 8) panPowerUnit[i] = new wxPanel(this, wxID_ANY, wxPoint(23, 20 + i*92), wxSize(521, 80), wxSUNKEN_BORDER);
		else panPowerUnit[i] = new wxPanel(this, wxID_ANY, wxPoint(566, 20 + (i - 8)*92), wxSize(522, 80), wxSUNKEN_BORDER);

		//Draw PSU titles
		if (i < 9) txtPowerUnitLabel[i] = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("Zasilacz ") + wxString::Format(wxT("%i"), i + 1), wxPoint(20, 31), wxSize(120, 20), wxALIGN_LEFT);
		else txtPowerUnitLabel[i] = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("Zasilacz ") + wxString::Format(wxT("%i"), i + 1), wxPoint(17, 31), wxSize(120, 20), wxALIGN_LEFT);
		txtPowerUnitLabel[i]->SetFont(wxFont(10, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

		//Draw panel divisor lines
		wxStaticLine* lineV1 = new wxStaticLine(panPowerUnit[i], wxID_ANY, wxPoint(120, 0), wxSize(1, 79), wxLI_VERTICAL);
		wxStaticLine* lineV2 = new wxStaticLine(panPowerUnit[i], wxID_ANY, wxPoint(210, 0), wxSize(1, 79), wxLI_VERTICAL);
		wxStaticLine* lineV3 = new wxStaticLine(panPowerUnit[i], wxID_ANY, wxPoint(335, 0), wxSize(1, 79), wxLI_VERTICAL);
		lineV1->SetBackgroundColour(wxColour(186, 186, 186));
		lineV2->SetBackgroundColour(wxColour(186, 186, 186));
		lineV3->SetBackgroundColour(wxColour(186, 186, 186));

		//Draw coil information
		if (i < NUM_POWER_UNITS - 2)
		{
			CoilNum = 0;

			//Check which coils are connected to the PSU
			for (uint8_t j = 0; j < NUM_COILS - 2; j++)
			{
				if (CEW[j][0] == i + 1)
				{
					PSUCoils[CoilNum] = j + 1;
					CoilNum++;
				}
			}

			if (CoilNum >= 2) //draw information for multiple coils
			{
				lineH1 = new wxStaticLine(panPowerUnit[i], wxID_ANY, wxPoint(120, 39), wxSize(90, 1), wxLI_HORIZONTAL);
				lineH1->SetBackgroundColour(wxColour(186, 186, 186));

				//Coil 1
				if (PSUCoils[0] > 10) coil1 = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("Cewka ") + wxString::Format(wxT("%i"), PSUCoils[0] - 10) + _T("\nDolna"), wxPoint(130, 4), wxSize(70, 40), wxALIGN_CENTRE_HORIZONTAL);
				else coil1 = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("Cewka ") + wxString::Format(wxT("%i"), PSUCoils[0]) + _T("\nGórna"), wxPoint(130, 4), wxSize(70, 40), wxALIGN_CENTRE_HORIZONTAL);
				coil1->SetFont(wxFont(9, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));

				//Coil 2
				if (PSUCoils[1] > 10) coil2 = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("Cewka ") + wxString::Format(wxT("%i"), PSUCoils[1] - 10) + _T("\nDolna"), wxPoint(130, 44), wxSize(70, 40), wxALIGN_CENTRE_HORIZONTAL);
				else coil2 = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("Cewka ") + wxString::Format(wxT("%i"), PSUCoils[1]) + _T("\nGórna"), wxPoint(130, 44), wxSize(70, 40), wxALIGN_CENTRE_HORIZONTAL);
				coil2->SetFont(wxFont(9, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
			}
			else //draw information for single coil
			{
				if (PSUCoils[0] > 10) coil1 = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("Cewka ") + wxString::Format(wxT("%i"), PSUCoils[0] - 10) + _T("\nDolna"), wxPoint(130, 24), wxSize(70, 40), wxALIGN_CENTRE_HORIZONTAL);
				else coil1 = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("Cewka ") + wxString::Format(wxT("%i"), PSUCoils[0]) + _T("\nGórna"), wxPoint(130, 24), wxSize(70, 40), wxALIGN_CENTRE_HORIZONTAL);
				coil1->SetFont(wxFont(10, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
			}
		}
		else
		{
			//Draw titles for coils 21 and 22
			if (i == NUM_POWER_UNITS - 2) coil1 = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("Cewka 21"), wxPoint(130, 31), wxSize(70, 40), wxALIGN_CENTRE_HORIZONTAL);
			else coil1 = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("Cewka 22"), wxPoint(130, 31), wxSize(70, 40), wxALIGN_CENTRE_HORIZONTAL);
			coil1->SetFont(wxFont(10, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
		}

		//Draw current readings
		txtStatusPSUOffset = wxPoint(0, 6);
		txtStatusPSUStartPosition = wxPoint(211, 31);
		txtStatusPSU[i] = new wxStaticText(panPowerUnit[i], wxID_ANY, _T(" "), txtStatusPSUStartPosition, wxSize(125, 80), wxALIGN_CENTRE_HORIZONTAL);
		txtStatusPSU[i]->SetFont(wxFont(10, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));

		//Set-current timeout message
		txtSetCurrentTimeout[i] = new wxStaticText(panPowerUnit[i], wxID_ANY, msgSetCurrentTimeout, txtStatusPSUStartPosition + wxPoint(0, 14), wxSize(125, 80), wxALIGN_CENTRE_HORIZONTAL);
		txtSetCurrentTimeout[i]->SetFont(wxFont(8, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
		txtSetCurrentTimeout[i]->SetForegroundColour(wxColour(220, 0, 0));
		txtSetCurrentTimeout[i]->Hide();

		//Draw current settings and units
		txtCtrlCurrentSetting[i] = new wxTextCtrl(panPowerUnit[i], wxID_ANY, "0", wxPoint(355, 24), wxSize(75, 30), wxALIGN_CENTRE_HORIZONTAL);
		txtCurrentUnits = new wxStaticText(panPowerUnit[i], wxID_ANY, _T("A"), wxPoint(431, 31), wxSize(20, 20), wxALIGN_CENTRE_HORIZONTAL);

		//Draw confirmation buttons
	    btnConfirmSetting[i] = new wxButton(panPowerUnit[i], 10001 + i, _T("OK"),  wxPoint(460, 24), wxSize(40, 30));
	    btnConfirmSetting[i]->SetBackgroundColour(wxColour(220, 220, 220));
	}

    //COIL STATUSES
	for (uint8_t i = 0; i < NUM_COILS; i++)
	{
		//Draw panels
		panCoilStatus[i] = new wxPanel(this, wxID_ANY, wxPoint(143 + i*43, 786), wxSize(42, 40), wxSUNKEN_BORDER);

		//Draw panel labels
		if (i < 9) txtCoilLabel = new wxStaticText(this, wxID_ANY, wxString::Format(wxT("%i"), i + 1), wxPoint(158 + i*43, 764), wxSize(120, 20), wxALIGN_LEFT);
		else txtCoilLabel = new wxStaticText(this, wxID_ANY, wxString::Format(wxT("%i"), i + 1), wxPoint(155 + i*43, 764), wxSize(120, 20), wxALIGN_LEFT);
		txtCoilLabel->SetFont(wxFont(11, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));

		//Draw status texts
		txtCoilStatus[i] = new wxStaticText(panCoilStatus[i], wxID_ANY, _T(" "), wxPoint(10, 10), wxSize(20, 20), wxALIGN_CENTRE_HORIZONTAL);
		txtCoilStatus[i]->SetFont(wxFont(11, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

		 //DIAGRAMS
		imgCoolingError[i] = new wxStaticBitmap(panCoilStatus[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) + wxString("/eclipse-workspace/ZasilanieCewek/Images/exclamation_mark.png"),
																		  wxBITMAP_TYPE_PNG), wxPoint(2, 2), wxSize(35, 35));
		imgNoSensor[i] = new wxStaticBitmap(panCoilStatus[i], wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) + wxString("/eclipse-workspace/ZasilanieCewek/Images/cross.png"),
				  wxBITMAP_TYPE_PNG), wxPoint(2, 2), wxSize(35, 35));

		imgCoolingError[i]->Hide();
		imgNoSensor[i]->Hide();
	}

	//Draw coil status title
    txtCoilStatusTitle = new wxStaticText(this, wxID_ANY, _T("Chłodzenie\nCewek:"), wxPoint(24, 788), wxSize(120, 20), wxALIGN_CENTRE_HORIZONTAL);
    txtCoilStatusTitle->SetFont(wxFont(10, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

	//Bind buttons to single event handler
    Bind(wxEVT_BUTTON, &cMain::OnButtonConfirmSetting, this, BTN_ID_START, BTN_ID_END);

    //Start display refresh thread
	if (THREAD_REFRESH_DISPLAY)
	{
	    threadRefreshDisplay = new cThreadRefreshDisplay(this);

	    if (threadRefreshDisplay->Run() != wxTHREAD_NO_ERROR)
	    {
	        wxLogError("Can't create display refresh thread!");
	        delete threadRefreshDisplay;
	        threadRefreshDisplay = nullptr;
	    }
	}
}

void cMain::RefreshDisplay(void)
{
	/******************************************* Copy Modbus Data ******************************************/

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);

		bModbusError = mbMaster->bModbusError;

		if (!bModbusError)
		{
			//Current and polarity errors
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
			{
				Current[i] = mbMaster->Current[i];
				if (i < NUM_POWER_UNITS - 2) bPolarityErrors[i] = mbMaster->bPolarityErrors[i];
			}

			//Bit registers
			STA[0] = mbMaster->STA[0];
			STA[1] = mbMaster->STA[1];
			STA[2] = mbMaster->STA[2];
			STA[3] = mbMaster->STA[3];
			ZNA = mbMaster->ZNA;

			//Errors
			ErrorsEcho = mbMaster->ErrorsEcho;
			ErrorsUART = mbMaster->ErrorsUART;
			ErrorsCRLF = mbMaster->ErrorsCRLF;
		}
	}

	/******************************************* Update Panels *********************************************/

	{
		wxCriticalSectionLocker main_lock(main_guard);
		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++) bCoolingError[i] = false;
	}

	if (bModbusError)
	{
		msgUserMessage = msgModbusError;

		//Update PSU Panels
		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			txtStatusPSUPosition = txtStatusPSUStartPosition;
			if (txtCtrlCurrentSetting[i]->IsEnabled()) txtCtrlCurrentSetting[i]->Disable();
			if (btnConfirmSetting[i]->IsEnabled()) btnConfirmSetting[i]->Disable();
			txtStatusPSU[i]->SetFont(wxFont(9, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
			txtStatusPSU[i]->SetForegroundColour(wxColour(220, 0, 0));
			txtStatusPSU[i]->SetLabel(msgUserMessage);
		}

		//Update Coil Status Panels
		for (uint8_t i = 0; i < NUM_COILS; i++)
		{
			txtCoilStatus[i]->SetLabel("-");
			if (imgNoSensor[i]->IsShown()) imgNoSensor[i]->Hide();
			if (imgCoolingError[i]->IsShown()) imgCoolingError[i]->Hide();
		}
	}
	else
	{
		/****************************************** COIL PANELS *******************************************/

		for (uint8_t i = 0; i < NUM_COILS - 2; i++)
		{
			if (CEW[i][2] == -1) //NO COOLING SENSOR
			{
				if (!imgNoSensor[i]->IsShown()) imgNoSensor[i]->Show();
				if (imgCoolingError[i]->IsShown()) imgCoolingError[i]->Hide();
				txtCoilStatus[i]->SetLabel(" ");
			}
			else if ((STA[CEW[i][1]] >> CEW[i][2] & 1) == 0) //COOLING OK
			{
				if (imgNoSensor[i]->IsShown()) imgNoSensor[i]->Hide();
				if (imgCoolingError[i]->IsShown()) imgCoolingError[i]->Hide();
				txtCoilStatus[i]->SetLabel("OK");
			}
			else if ((STA[CEW[i][1]] >> CEW[i][2] & 1) == 1) //COOLING PROBLEM
			{
				if (imgNoSensor[i]->IsShown()) imgNoSensor[i]->Hide();
				if (!imgCoolingError[i]->IsShown()) imgCoolingError[i]->Show();
				txtCoilStatus[i]->SetLabel(" ");

				{
					//Store PSU cooling errors
					wxCriticalSectionLocker main_lock(main_guard);
					bCoolingError[CEW[i][0] - 1] = true;
				}

				if (Current[CEW[i][0] - 1] != 0)
				{
					//Zero current
					threadSetCurrent[CEW[i][0] - 1] = new cThreadSetCurrent(this, mbMaster, CEW[i][0], 0);

					if (threadSetCurrent[CEW[i][0] - 1]->Run() != wxTHREAD_NO_ERROR)
					{
						wxLogError("Can't create set-current thread!");
						delete threadSetCurrent[CEW[i][0] - 1];
						threadSetCurrent[CEW[i][0] - 1] = NULL;
					}
				}
			}
		}

		//TODO: update Coil 21 and 22 status, and check for cooling problems
		txtCoilStatus[20]->SetLabel("-");
		txtCoilStatus[21]->SetLabel("-");

		/****************************************** PSU PANELS ********************************************/

		//Update PSU Panels
		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			msgUserMessage.Empty();
			txtStatusPSUPosition = txtStatusPSUStartPosition;

			//Check if PSU is excluded
			if (i < NUM_POWER_UNITS - 2 && ADR[i][0] == 0)
			{
				msgUserMessage = msgPSUExcluded;
				txtStatusPSUPosition -= txtStatusPSUOffset + wxPoint(0, 1);
				txtStatusPSU[i]->SetForegroundColour(wxColour(245, 117, 0));
				txtStatusPSU[i]->SetFont(wxFont(9, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
				if (txtCtrlCurrentSetting[i]->IsEnabled()) txtCtrlCurrentSetting[i]->Disable();
				if (btnConfirmSetting[i]->IsEnabled()) btnConfirmSetting[i]->Disable();
			}
			else //PSU not excluded
			{
				if (bCoolingError[i])
				{
					msgUserMessage = msgCoolingError;
					txtStatusPSU[i]->SetForegroundColour(wxColour(220, 0, 0));
					txtStatusPSU[i]->SetFont(wxFont(9, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
					if (txtCtrlCurrentSetting[i]->IsEnabled()) txtCtrlCurrentSetting[i]->Disable();
					if (btnConfirmSetting[i]->IsEnabled()) btnConfirmSetting[i]->Disable();
				}

				if (i < NUM_POWER_UNITS - 2)
				{
					//Check for polarity errors (PSU 1-14)
					if (bPolarityErrors[i])
					{
						//Update message
						if (msgUserMessage.IsEmpty()) msgUserMessage += msgPolarityError;
						else
						{
							txtStatusPSUPosition -= txtStatusPSUOffset;
							msgUserMessage += "\n" + msgPolarityError;
						}

						txtStatusPSU[i]->SetForegroundColour(wxColour(220, 0, 0));
						txtStatusPSU[i]->SetFont(wxFont(8, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
						if (txtCtrlCurrentSetting[i]->IsEnabled()) txtCtrlCurrentSetting[i]->Disable();
						if (btnConfirmSetting[i]->IsEnabled()) btnConfirmSetting[i]->Disable();
					}
				}

				//Check for ECHO, CRLF, or UART errors
				if (((ErrorsEcho >> i) & 1) || ((ErrorsCRLF >> i) & 1) || ((ErrorsUART >> i) & 1))
				{
					//CRLF Errors
					if ((ErrorsCRLF >> i) & 1)
					{
						if (msgUserMessage.IsEmpty()) msgUserMessage += msgCRLFError;
						else
						{
							txtStatusPSUPosition -= txtStatusPSUOffset;
							msgUserMessage += "\n" + msgCRLFError;
						}
					}

					//ECHO Errors
					if ((ErrorsEcho >> i) & 1)
					{
						if (msgUserMessage.IsEmpty()) msgUserMessage += msgEchoError;
						else
						{
							txtStatusPSUPosition -= txtStatusPSUOffset;
							msgUserMessage += "\n" + msgEchoError;
						}
					}

					//UART Errors
					if ((ErrorsUART >> i) & 1)
					{
						if (msgUserMessage.IsEmpty()) msgUserMessage += msgUARTError;
						else
						{
							txtStatusPSUPosition -= txtStatusPSUOffset;
							msgUserMessage += "\n" + msgUARTError;
						}
					}

					txtStatusPSU[i]->SetForegroundColour(wxColour(220, 0, 0));
					txtStatusPSU[i]->SetFont(wxFont(8, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
					if (txtCtrlCurrentSetting[i]->IsEnabled()) txtCtrlCurrentSetting[i]->Disable();
					if (btnConfirmSetting[i]->IsEnabled()) btnConfirmSetting[i]->Disable();
				}

				//No errors - display current
				if (msgUserMessage.IsEmpty())
				{
					//Update message with current reading
					msgUserMessage = wxString::Format(wxT("%.1f"), Current[i]) + "A";

					//Check for negative polarity
					if ((STA[ADR[i][1]] >> ADR[i][2]) & 1)
					{
						msgUserMessage = wxString("-") + msgUserMessage;
						txtStatusPSUPosition -= wxPoint(2, 0);
					}

					txtStatusPSU[i]->SetFont(wxFont(10, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
					txtStatusPSU[i]->SetForegroundColour(wxColour());

					if (eUserMode == Local)
					{
						if (threadSetCurrent[i] != nullptr) //disable if set-current thread already exists
						{
							if (txtCtrlCurrentSetting[i]->IsEnabled()) txtCtrlCurrentSetting[i]->Disable();
							if (btnConfirmSetting[i]->IsEnabled()) btnConfirmSetting[i]->Disable();
						}
						else
						{
							if (!txtCtrlCurrentSetting[i]->IsEnabled()) txtCtrlCurrentSetting[i]->Enable();
							if (!btnConfirmSetting[i]->IsEnabled()) btnConfirmSetting[i]->Enable();
						}
					}
					else if (eUserMode == Remote)
					{
						if (txtCtrlCurrentSetting[i]->IsEnabled()) txtCtrlCurrentSetting[i]->Disable();
						if (btnConfirmSetting[i]->IsEnabled()) btnConfirmSetting[i]->Disable();
					}

					if (bSetCurrentTimeout[i])
					{
						txtStatusPSUPosition -= wxPoint(0, 8);
						txtSetCurrentTimeout[i]->Show();
					}
					else txtSetCurrentTimeout[i]->Hide();
				}
				else if (bSetCurrentTimeout[i]) //append set-current timeout message
				{
					txtStatusPSUPosition -= txtStatusPSUOffset;
					msgUserMessage += "\n" + msgSetCurrentTimeout;
				}
			}

			txtStatusPSU[i]->SetPosition(txtStatusPSUPosition);
			if (txtStatusPSU[i]->GetPosition() == (txtStatusPSUStartPosition - txtStatusPSUOffset))
				txtStatusPSU[i]->SetPosition(txtStatusPSU[i]->GetPosition() + wxPoint(0, 2));
			txtStatusPSU[i]->SetLabel(msgUserMessage);
		}
	}
}

void cMain::OnRefreshDisplay(wxCommandEvent& evt)
{
	RefreshDisplay();
}

/* Menu clicked event */
void cMain::OnMenuClickedInformation(wxCommandEvent& evt)
{
    if (informationWindowIsOpen) //window already open
    {
        pInformation->Raise(); //show window
    }
    else
    {
        informationWindowIsOpen = true;
        pInformation = new cInformation(this);
        pInformation->SetPosition(this->GetPosition());
        pInformation->SetIcon(this->GetIcon());
        pInformation->Show();
    }

    evt.Skip();
}

/* Menu clicked event */
void cMain::OnMenuClickedMode(wxCommandEvent& evt)
{
	if (eUserMode == Local)
	{
	    this->SetTitle(_T("Zasilanie Cewek Korekcyjnych [Sterowanie Zdalne]"));
		mOptionsMenu->SetLabel(wxID_NETWORK, _T("Sterowanie Lokalne"));
		eUserMode = Remote;
	}
	else if (eUserMode == Remote)
	{
	    this->SetTitle(_T("Zasilanie Cewek Korekcyjnych"));
		mOptionsMenu->SetLabel(wxID_NETWORK, _T("Sterowanie Zdalne"));
		eUserMode = Local;
	}

	//Store user mode in Modbus mapping
	wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);
	mbMaster->mb_mapping->tab_registers[HOLDING_MODE - 1] = (uint16_t) eUserMode;

    evt.Skip();
}

/* Frame closed - Modbus thread destruction */
void cMain::OnClose(wxCloseEvent&)
{
	TerminateApp();
}

/* Frame closed from menu - Modbus thread destruction */
void cMain::OnMenuClickedExit(wxCommandEvent& evt)
{
	TerminateApp();
}

void cMain::TerminateApp()
{
	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);

		bModbusError = mbMaster->bModbusError;

		if (!bModbusError)
		{
			//Copy current readings
			for (uint8_t i = 0; i < NUM_POWER_UNITS; i++) Current[i] = mbMaster->Current[i];
		}
	}

	if (!bModbusError)
	{
		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			txtStatusPSU[i]->SetForegroundColour(wxColour());
			txtStatusPSU[i]->SetFont(wxFont(9, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
			txtStatusPSU[i]->SetLabel(_T("Zerowanie Prądu..."));

			if (Current[i] != 0)
			{
				//Zero current
				threadSetCurrent[i] = new cThreadSetCurrent(this, mbMaster, i + 1, 0);

				if (threadSetCurrent[i]->Run() != wxTHREAD_NO_ERROR)
				{
					wxLogError("Can't create set-current thread!");
					delete threadSetCurrent[i];
					threadSetCurrent[i] = NULL;
				}
			}
		}

		//Wait for all set-current threads to finish
		for (uint8_t i = 0; i < NUM_POWER_UNITS; i++)
		{
			while (1)
			{
				if (!threadSetCurrent[i]) break;
				wxThread::This()->Sleep(1);
			}
		}
	}

	//Delete display refresh thread
	if (threadRefreshDisplay)
	{
		if (threadRefreshDisplay->Delete() != wxTHREAD_NO_ERROR) wxLogError("Can't delete display refresh thread!");
	}

	while (1)
	{
		if (!threadRefreshDisplay) break;
		wxThread::This()->Sleep(1);
	}

	//Close information window
	if (informationWindowIsOpen) pInformation->Close();

	Destroy();
}
