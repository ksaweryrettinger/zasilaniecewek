#pragma once

#include "wx/wx.h"
#include <limits.h>

//APPLICATION MODES
typedef enum { Local = 0, Remote = 1} eMode;

//GLOBAL VARIABLES
extern int8_t ADR[14][6];
extern int8_t CEW[20][3];
extern int32_t MAX_CURRENT;

//ENABLE THREADS
const int32_t THREAD_REFRESH_DISPLAY = 1;

//EVENT IDs
const int32_t BTN_ID_START  = 10001;
const int32_t BTN_ID_END = 10016;

//THREAD REFRESH TIMES
const int32_t DISPLAY_REFRESH_TIME = 100;

//TIMEOUTS AND DELAYUS
const int32_t ZERO_CURRENT_TIMEOUT    = 10;  //s
const int32_t ZERO_CURRENT_DELAY      = 250; //ms
const int32_t SWITCH_POLARITY_TIMEOUT = 10;  //s
const int32_t SWITCH_POLARITY_DELAY   = 100; //ms

//MODBUS CONFIGURATION
const int32_t MB_BAUD = 921600;
const int32_t MB_RTU_SLEEP = 100; //100ms between read operations
const int32_t MB_RESPONSE_TIMEOUT_SEC = 0;
const int32_t MB_RESPONSE_TIMEOUT_US = 250000; //250ms timeout

//MODBUS DATA AND ADDRESSING
const int32_t MB_HOLDING_ADDRESS = 40001;
const int32_t MB_HOLDING_NUMREG  = 24;

//MODBUS HOLDING REGISTER NUMBERING
const int32_t HOLDING_CURRENT_START = 1;
const int32_t HOLDING_CURRENT_END   = 16;
const int32_t HOLDING_UART_ERRORS   = 17;
const int32_t HOLDING_ECHO_ERRORS   = 18;
const int32_t HOLDING_CRLF_ERRORS   = 19;
const int32_t HOLDING_STA0 			= 20;
const int32_t HOLDING_STA1 			= 21;
const int32_t HOLDING_STA2 			= 22;
const int32_t HOLDING_STA3 			= 23;
const int32_t HOLDING_ZNA 			= 24;
const int32_t HOLDING_MBERRORS 		= 25;
const int32_t HOLDING_MODE	 		= 26;

//OTHER
const int32_t NUM_POWER_UNITS = 16;
const int32_t NUM_COILS       = 22;
const int32_t MAX_READ_CHARS  = 7;
