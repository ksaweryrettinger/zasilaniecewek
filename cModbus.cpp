#include "cModbus.h"

cModbus::cModbus(cEntry* pEntry, cMain* pMain)
{
    this->pMain = pMain;

    //Copy Modbus configuration
    mbAddress = pEntry->mbAddress;
    mbPort = pEntry->mbPort;

	//Copy Bit Register addresses
	ZNA_Address = pEntry->ZNA_Address;
	for (uint8_t i = 0; i < 4; i++) STA_Address[i] = pEntry->STA_Address[i];

    ctRTU = -1;
    ctxRTU = nullptr;
    bStartMain = false;
    bModbusError = true;
	ZNA = 0;
	ErrorsEcho = 0;
	ErrorsUART = 0;
	ErrorsCRLF = 0;

	/* Holding 1-16	= Current in BCD format
	 * Holding 17	= UART errors
	 * Holding 18   = ECHO errors
	 * Holding 19   = CRLF errors
	 * Holding 20   = STA0
	 * Holding 21	= STA1
	 * Holding 22	= STA2
	 * Holding 23	= STA3
	 * Holding 24   = ZNA
	 * Holding 25   = Modbus RTU Errors
	 * Holding 26   = Application Mode
	 *
	 * TODO: add ADR and CEW configurations to mapping
	 * TODO: add local/remote mode to mapping
	 */

    mb_mapping = modbus_mapping_new_start_address(1, 0, 10001, 0, MB_HOLDING_ADDRESS, MB_HOLDING_NUMREG + 2, 30001, 0);

    //Start Modbus RTU thread
    mbThreadReadPSU = new cThreadReadPSU(this);
    mbThreadWritePSU = nullptr;
    mbThreadSetPolarity = nullptr;

    if (mbThreadReadPSU->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create Modbus RTU thread!");
        delete mbThreadReadPSU;
        mbThreadReadPSU = nullptr;
    }

	pMain->mbMaster = this;
}

void cModbus::CloseModbus(void)
{
    {
        wxCriticalSectionLocker mb_lock(mb_guard);

        //Delete RTU thread
        if (mbThreadReadPSU)
        {
            if (mbThreadReadPSU->Delete() != wxTHREAD_NO_ERROR) wxLogError("Can't delete PSU Read thread!");
        }
    }

    while (1)
    {
        {
            wxCriticalSectionLocker mb_lock(mb_guard);
            if (!mbThreadReadPSU) break;
        }

        wxThread::This()->Sleep(1);
    }
}

//Set current in single power unit
void cModbus::SetPowerUnitCurrent(uint8_t unitID, float current)
{
	//Create new thread for writing new current value to PSU
	mbThreadWritePSU = new cThreadWritePSU(this, unitID, current);

    if (mbThreadWritePSU->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create PSU Write thread!");
        delete mbThreadWritePSU;
        mbThreadWritePSU = nullptr;
    }
}

//Set polarity of single power unit
void cModbus::SetPowerUnitPolarity(uint8_t unitID, bool bSetNegative)
{
	//Create new thread for setting PSU polarity
	mbThreadSetPolarity = new cThreadSetPolarity(this, unitID, bSetNegative);

    if (mbThreadSetPolarity->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create Modbus PSU Set Polarity thread!");
        delete mbThreadSetPolarity;
        mbThreadSetPolarity = nullptr;
    }
}
