#pragma once

#include <string.h>
#include <locale>
#include <queue>
#include "wx/wx.h"
#include <wx/statline.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "cModbus.h"
#include "cThreadSetCurrent.h"
#include "cThreadRefreshDisplay.h"
#include "cInformation.h"
#include "constants.h"

class cEntry;
class cModbus;
class cConstants;
class cInformation;
class cThreadTPG362;
class cThreadSetCurrent;
class cThreadRefreshDisplay;

class cMain : public wxFrame
{
	public: //constructor
		cMain(cEntry*);

	private: //event handlers
		void OnMenuClickedInformation(wxCommandEvent& evt);
		void OnMenuClickedMode(wxCommandEvent& evt);
		void OnMenuClickedExit(wxCommandEvent& evt);
		void OnRefreshDisplay(wxCommandEvent& evt);
		void OnButtonConfirmSetting(wxCommandEvent& evt);
		void OnClose(wxCloseEvent& evt);
		void OnModbusWriteError(wxTimerEvent& evt);

	private: //other methods
		void RefreshDisplay(void);
		void TerminateApp(void);

	public: //class pointers
		cEntry* pEntry;
		cModbus* mbMaster;
		wxMenu* mHelpMenu;
		cInformation* pInformation;
		bool informationWindowIsOpen;

	public: //other public variables
		wxCriticalSection main_guard; //cMain data guard
		eMode eUserMode;
		bool bSetCurrentTimeout[NUM_POWER_UNITS] = { false };

	private: //menu
		wxMenuBar* mMenuBar;
		wxMenu* mOptionsMenu;

	private: //UI elements

		//FONTS
		wxFont myFont;

		//PANELS
		wxPanel* panCoilStatus[NUM_COILS];
		wxPanel* panPowerUnit[NUM_POWER_UNITS];

		//IMAGES
		wxStaticBitmap* imgCoolingError[NUM_COILS];
		wxStaticBitmap* imgNoSensor[NUM_COILS];

		//STATIC TEXTS
		wxStaticText* txtCurrentUnits;
		wxStaticText* txtCoilStatusTitle;
		wxStaticText* txtCoilLabel;
		wxStaticText* txtCoilStatus[NUM_COILS];
		wxStaticText* txtPowerUnitLabel[NUM_POWER_UNITS];
		wxStaticText* txtStatusPSU[NUM_POWER_UNITS];
		wxStaticText* txtSetCurrentTimeout[NUM_POWER_UNITS];

		//USER MESSAGES
		wxPoint txtStatusPSUStartPosition;
		wxPoint txtStatusPSUPosition;
		wxPoint txtStatusPSUOffset;
		wxString msgUserMessage;
		wxString msgPSUExcluded;
		wxString msgModbusError;
		wxString msgEchoError;
		wxString msgUARTError;
		wxString msgCRLFError;
		wxString msgPolarityError;
		wxString msgCoolingError;
		wxString msgSetCurrentTimeout;

		//CONTROL TEXT AND BUTTONS
		wxTextCtrl* txtCtrlCurrentSetting[NUM_POWER_UNITS];
		wxButton* btnConfirmSetting[NUM_POWER_UNITS];

		//THREADS
		cThreadSetCurrent* threadSetCurrent[NUM_POWER_UNITS] = { NULL };
		cThreadRefreshDisplay* threadRefreshDisplay = NULL;

	private:

		//Modbus data
		float Current[NUM_POWER_UNITS] = { 0 };
		uint16_t STA[4] = { 0 };
		uint16_t ZNA;
		uint16_t ErrorsEcho;
		uint16_t ErrorsUART;
		uint16_t ErrorsCRLF;
		bool bPolarityErrors[NUM_POWER_UNITS - 2] = { false };
		bool bModbusError;

		//Other data
		bool bCoolingError[NUM_POWER_UNITS];

		//SYSTEM CLOCK
		friend class cThreadRefreshDisplay;
		friend class cThreadSetCurrent;

		wxDECLARE_EVENT_TABLE();
};


